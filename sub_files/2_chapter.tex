\chapter{Feedforward Neural Network}
\label{chap:chap3}

A \textbf{feedforward neural network} is a parametric model aiming at finding the best approximation $\hat{f}$ of some function $f(\bm{x})$ where $\bm{x} = (x_1, x_2, \dots, x_p) \in \mathbb{R}^{p}$ is a vector of $p$ independent variables and $y \in \mathbb{R}$ is a dependent variable. A neural network can potentially approximate any continuous function [\cite{leshno1993multilayer}].

The way a neural network searches the best approximation of $f$ can be decomposed into two phases: \textit{build phase}, where the network is defined, and \textit{training phase}, where the data flows through the network and the form of $\hat{f}$ is updated.

\section{Build phase}
To understand a neural network, it is necessary to describe its essential components and how they interact within the network. As an example, consider
\begin{equation}\label{eq:eq21}
\hat{f}(\bm{x})=\ell_4(\ell_3(\ell_2(\ell_1(\bm{x})))):
\end{equation}
the functions $\ell_1, \ell_2, \ell_3$ are called \textit{hidden layers} (respectively \textit{first}, \textit{second} and \textit{third layer}) and $\ell_4$ is called \textit{output layer}. The network can be expanded by adding an arbitrary number of hidden layers while the output layer is always unique.

\subsection{Layer} Any layer of a network can be defined as
\begin{equation}\label{eq:eq22}
\ell(\bm{z}, h; \bm{W}, \bm{b}) = h(\bm{W}\bm{z} + \bm{b}),
\end{equation}
where
\begin{itemize}\itemsep0pt
	\item $\bm{z} \in \mathbb{R}^{m}$ is the vector of the $m$ output variables from the previous layer\footnote{The input vector $X$ of the network, also represents a layer, called \textit{input layer}. However no computation is made within it.};
	\item $\bm{W} \in \mathbb{R}^{m \times d}$ is a matrix of parameters ($d$ is named \textit{width} of the layer);
	\item $\bm{b} \in \mathbb{R}^d$ is a vector called \textit{bias term} or \textit{intercept term};
	\item $h: \mathbb{R} \rightarrow \mathbb{R}$ is a continuous function, called \textit{activation function}. The next section presents some examples of activation function. It is important to remark that when $h$ is applied to a vector argument, like in \ref{eq:eq22}, it is meant to work element-by-element.
\end{itemize}


\subsection{Activation functions} In literature [\cite{clevert2015fast}] [\cite{glorot2011deep}], the mostly used activation functions for hidden layers are the $\relu$ function (Fig. \ref{fig:fig21}), the $\elu$ function (Fig. \ref{fig:fig22}) and the \textit{sigmoid} function (Fig. \ref{fig:fig23}).
\newpage

\begin{figure}[!hb]
	\centering
	\includegraphics[width=1\textwidth, height=0.3\textheight]{../images/relu_function.png}
	\caption{Rectified Linear Unit(ReLU)}
	\label{fig:fig21}
	\small
	\centering
	\begin{align}\label{eq:eq23}
	\relu(z) = \left\{ \begin{array}{cc} 
	0 & \hspace{5mm} se\ z < 0\\
	z & \hspace{5mm} se\ z \geq 0 \\
	\end{array} \right.
	\end{align}
\end{figure}
\begin{figure}[!h]
	\centering
	\includegraphics[width=1\textwidth, height=0.3\textheight]{../images/elu_function.png}
	\caption{Exponential Linear Unit(ELU), for different values of $\alpha$}
	\label{fig:fig22}
	\small
	\centering
	\begin{align}\label{eq:eq24}
	\elu(z) = \left\{ \begin{array}{cc} 
	\alpha(exp(z) - 1) & \hspace{5mm} se\ z < 0\\
	z & \hspace{5mm} se\ z \geq 0 \\
	\end{array} \right.
	\end{align}
\end{figure}
\begin{figure}[!h]
	\centering
	\includegraphics[width=1\textwidth, height=0.3\textheight]{../images/sigmoid_function.png}
	\caption{Sigmoid}
	\label{fig:fig23}
	\small
	\centering
	\begin{align}\label{eq:eq25}
	\sigma(z) = \frac{\exp(z)}{1 + \exp(z)}
	\end{align}
\end{figure}
\newpage
Regarding the output layer, the choice of the activation function depends on the nature of the problem\footnote{A neural network can be used to model different kind of dependent variables. The package is currently implemented only for regression and classification problems.}.

For regression problems the identity function is typically used, so that the output layer is equivalent to a linear transformation of the data coming in input to it.

In classification problems there is a distinction between binary and multicategorical classification: in the binary case the \textit{sigmoid} function is the typical choice; in the multicategory case, it is commonly used the \textit{multinomial logistic}, also called \textit{softmax} function:
\begin{equation}\label{eq:eq26}
h(z_j)= \frac{\exp(z_j)}{\sum_{j=1}^J \exp(z_j)},\ \ j=1,\dots,J.
\end{equation}
\newpage
\subsection{Cost function}
Considering a neural network structure built with the ingredients explained above, once initialized all parameters to small random numbers [\cite{andrew_ng_notes}], the output can be computed. But how close is it be to the true $y$ value? A \textit{cost function} (also called \textit{loss function}) can be used to answer this question. As well as the activation function for the output layer, the choice of the cost function is strictly related to the type of outcome.

In regression problems, the \textit{mean squared error} (MSE) is the mostly commonly used loss function:
\begin{equation}\label{eq:eq27}
\mse(y, \hat{y})=\mathbb{E}\big{[}y - \hat{y}\big{]}^2.
\end{equation}
In classification problems the \textit{negative cross entropy} (NCE) is frequently employed:
\begin{itemize}
\item [$\bullet$] In the binary case, the NCE is
\begin{equation}\label{eq:eq28}
-\mathbb{E}\big{[}y\log(\hat{y}) + (1-y)\log(1 - \hat{y})\big{]};
\end{equation}
\item [$\bullet$] in the multiclass case, the NCE takes the form
\begin{equation}\label{eq:eq29}
-\mathbb{E}\bigg{[}\sum_{j=1}^{J}\big{[}y_j\log(\hat{y}_j) + (1 - y_j)\log(1 - \hat{y}_j)\big{]}\bigg{]}
\end{equation}
\item [where] $j=1, ..., J$ denotes the $j$-th class.
\end{itemize}
 

\section{Training phase}
An observation (also called \textit{example}) is an ordered pair $(\bm{x}^{(i)},\ y^{(i)})$ where:
\begin{itemize}
	\item [$\bullet$] $i = 1, \ldots, n$ denotes the unit.
	\item [$\bullet$] $\bm{x}^{(i)} \in \mathbb{R}^p$ is the vector of the $p$ \textit{features} (also called \textit{independent variables}, \textit{covariates} or \textit{regressors}) of the unit;
	\item [$\bullet$] $y^{(i)} \in \mathbb{R}$ is the \textit{target} (also called \textit{dependent variable}, \textit{output variable} or \textit{label}) of the unit;
\end{itemize}
%To explain the training phase, first a different point of view is introduced about the input vector $\bm{x} \in \mathbb{R}^p$ of the network that it can be seen as a matrix $X \in \mathbb{R}^{n \times p}$, where $n$ is the number of \textit{observations}.
\newpage
The training phase consists of \textit{iterations} where, in turn, at each iteration the following steps are executed:
\begin{itemize}\itemsep0pt
	\item take an example $(\bm{x}^{(i)},\ y^{(i)})$ as input;
	\item compute the output $\hat{y}^{(i)}$;
	\item compute the cost function comparing $\hat{y}^{(i)}$ with $y^{(i)}$;
	\item execute a \textit{training optimization algorithm} to updates the vector of all parameters of the networks $\bm{\theta}$.
\end{itemize}

The \textit{optimizer} used in the last step is typically the \textit{gradient descent} (GD) or one of its variants [\cite{ruder2016overview}].

\subsection{Gradient descent algorithm} The GD algorithm consists in a step where the parameters are updated as 
\begin{equation}\label{eq:eq210}
\bm{\theta}^{(k)} = \bm{\theta}^{(k-1)} - \eta\nabla_{\bm{\theta}}\mathcal{L}(y, \hat{y}),
\end{equation}
where
\begin{itemize}\itemsep0pt
	\item $\bm{\theta}$ denotes the parameters to be updated;
	\item $k$ denotes the $k$-th step;
	\item $\eta \in \mathbb{R}$ is the \textit{learning rate};
	\item $\mathcal{L}$ is the loss function;
	\item $\nabla_{\bm{\theta}}\mathcal{L}$ is the \textit{gradient} of the loss function w.r.t. $\bm{\theta}$.
\end{itemize}

The gradient of the loss function can be computed exactly and efficiently thanks to the \textit{backpropagation} algorithm, which exploits the chain-rule\footnote{Chain-rule: $$\frac{\partial z}{\partial x^\top} = \frac{\partial z}{\partial y^\top} \cdot \frac{\partial y}{\partial x^\top},$$ where $z$ is a function of $y$ and $y$ is a function of $x$, and this making $z$ to depend on $x$.} of derivatives. For more detail on the backpropagation algorithm, see [\cite{geron2017hands}, Appendix D.][\cite{riedmiller1993direct}].
